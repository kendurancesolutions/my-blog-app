# My Blog (Demo)
Demo combining a bare-bones React front-end with an ExpressJS/MongoDB backend.

### Instructions
1. Build the front-end: `npm run build`.
2. Copy `/build` to `/src` in the backend.

### Future Work
- Incorporating TailwindCSS