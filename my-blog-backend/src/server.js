import express from 'express'
import bodyParser from 'body-parser'
import { MongoClient } from 'mongodb'
import path from "path";
const app = express(); // ready to define endpoints and what to do when they're hit
app.use(express.static(path.join(__dirname, '/build')))
app.use(bodyParser.json())
const db_server   = `mongodb://localhost:27017`
const db_name     = `my-blog`
const collection_articles = `articles`
const api_prefix  = '/api/articles/'

const withDB = async ( operations, res ) => {
	try{
		const client  = await MongoClient.connect(db_server, { useNewUrlParser: true } )
		const db      = client.db(db_name)
		await operations(db)
		client.close()
	}catch(e){
		res.status(500).json({message: 'Error with db connection', error:e})
	}
}

/** GET all articles **/
app.get(api_prefix, async ( {params},res) => {
	withDB(async ( db ) => {
		const articleInfo = await db.collection(collection_articles).find().toArray()
		res.status(200).json(articleInfo) // better than send() when working with JSON data
	}, res)
})
/** GET one article **/
app.get(`${api_prefix}:name`, async ( {params},res) => {
	withDB( async ( db ) => {
		const articleInfo = await db.collection(collection_articles).findOne({ name:params.name })
		res.status(200).json(articleInfo) // better than send() when working with JSON data
	}, res)
})
/** PUT incremented upvotes **/
app.put(`${api_prefix}:name/upvote`, async ( {params},res) => {
	withDB( async ( db ) => {
		const articleInfo = await db.collection(collection_articles).findOne({ name:params.name })
		if(articleInfo){
			await db.collection(collection_articles).updateOne({ name:params.name}, {'$set': { upvotes: articleInfo.upvotes+1}})
			const updatedArticle = await db.collection(collection_articles).findOne({ name:params.name })
			res.status(200).json(updatedArticle)//.send(`${updatedArticle.name} now has ${updatedArticle.upvotes} upvote(s)!`)
		}
	}, res)
})
/** Send GET request to return current # of votes **/
app.get(`${api_prefix}:name/upvote`, async ( {params},res) => {
	withDB( async ( db ) => {
		const articleInfo = await db.collection(collection_articles).findOne({ name:params.name })
		res.send(`${articleInfo.name} has ${articleInfo.upvotes} upvote(s)!`)
	}, res)
})
/** Send POST request to add comments **/
app.post(`${api_prefix}:name/add-comment`, async ({params, body},res) => {
	withDB( async ( db ) => {
		const { username, text } = body
		const articleInfo = await db.collection(collection_articles).findOne({ name:params.name }) // GET
		if(articleInfo){
			await db.collection(collection_articles).updateOne({ name:params.name }, {'$set':
					{ comments: articleInfo.comments.concat({username, text}) }
			})  // add to list
			const updatedArticle = await db.collection(collection_articles).findOne({ name:params.name })
			res.status(200).json(updatedArticle)
		}
	}, res)
})
/** DELETE comments **/         // TODO: Doesn't work
app.delete(`${api_prefix}:name/delete-comment/:username`,({params}, res) => {
	withDB( async ( db ) => {
		const articleInfo = await db.collection(collection_articles).findOne({ name:params.name })
		await db.collection(collection_articles).updateOne({ name:params.name }, {'$set':
				{ comments: articleInfo.comments.filter((body) => {
					return body.username && body.username !== params.username
					}) }
		})  // add to list
		const updatedArticle = await db.collection(collection_articles).findOne({ name:params.name })
		res.status(200).json(updatedArticle.comments)
	}, res)
})
/** Send GET request to read comments **/
app.get(`${api_prefix}:name/comments`,({params}, res) => {
	withDB( async ( db ) => {
		const articleInfo = await db.collection(collection_articles).findOne({ name:params.name })
		res.json(articleInfo.comments)
	}, res)
})
/** Tell all non-api calls to our front-end routes **/
app.get('*', (req, res) => {
	res.sendFile(path.join(__dirname + `/build/index.html`))
})
/** Tell server where to go **/
app.listen(8000, () => { console.log("Listening on Port 8000") })
