import {Link} from "react-router-dom";


export const Navbar = () => {
  return(
	  <nav className="nav">
		  <Link to="/">Home</Link> |&nbsp;
		  <Link to="/about">About</Link> |&nbsp;
		  <Link to="/articles">Articles</Link>
	  </nav>
  )
}