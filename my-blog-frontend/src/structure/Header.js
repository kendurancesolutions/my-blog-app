import {Navbar} from "./Navbar";
import {useState} from "react";

export const Header = () => {
  return(
		<div className="header">
			<Navbar />
			<ThemeChanger />
		</div>
  )
}
const ThemeChanger = () => {
	const ls_color = localStorage.getItem("themeColorVar")
	const defaultColor = "#1abc9c"
	const [theme, setTheme] = useState(ls_color || defaultColor);

	const changeThemeColor = (newThemeColor, changingState) => {
		if(changingState) setTheme(newThemeColor)
		const ele = document.querySelector(":root")
		ele.style.setProperty("--theme-color", newThemeColor)
		localStorage.setItem("themeColorVar", theme)
	}

	changeThemeColor(theme, false)
	return(
		<div className="theme-selector-container">
			Change Theme:
			<input type="color" id="theme-selector" value={theme}
			       onChange={(val) => {
				       changeThemeColor(val.target.value, true)
						 }}/>
			<button onClick={() => { changeThemeColor(defaultColor, true) }}>Reset</button>
		</div>
	)
}