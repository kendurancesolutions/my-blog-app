import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import './App.css';
import {HomePage} from "./pages/HomePage";
import {AboutPage} from "./pages/AboutPage";
import {SingleArticle, ArticlesPage} from "./pages/ArticlesPage";
import {Header} from "./structure/Header";
import {Footer} from "./structure/Footer";
import {NotFound} from "./pages/NotFound";

function App() {
  return (
      <Router>
        <Header />
        <div className="App">
          <Switch>
            <Route path="/" component={HomePage} exact />
            <Route path="/about"    component={AboutPage} />
            <Route path="/articles" component={ArticlesPage} />
            <Route path="/article/:name"  component={SingleArticle} />
            <Route component={NotFound} /> {/*  Must be last if using switch */}
          </Switch>
        </div>
        <Footer />
      </Router>
  );
}

export default App;
