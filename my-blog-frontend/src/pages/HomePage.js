import {ArticleList} from "./ArticlesPage";
import articleContent from "../data/ArticleContent";
import {LoremIpsum} from "../structure/Utilities";


export const HomePage = () => {
	return (
		<>
			<h1>Lorem Ipsum Blog!</h1>
			<p>
				<LoremIpsum/>
			</p>
			<p>
				<LoremIpsum/>
			</p>
			<p>
				<LoremIpsum/>
			</p>
			<hr/>
			<h3>Latest Articles</h3>
			<ArticleList articleList={articleContent} />
		</>
	)
}