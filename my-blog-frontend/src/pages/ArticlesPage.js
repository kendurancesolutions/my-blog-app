import {useEffect, useState} from "react";
import articleContent from '../data/ArticleContent'
import {Link} from "react-router-dom";
import {NotFound} from "./NotFound";

// Articles
export const ArticlesPage = () => {

  return(
		<>
			<h1>Articles</h1>
			<ArticleList articleList={articleContent} />
		</>
  )
}

export const ArticleList = ({ articleList }) => {
	return (
		<ul className="article-list">
			{articleList.map((article, key) => {
				return <li key={key}>
					<strong>{article.title}</strong>
					{article.content && <><br/>{article.content[0].substring(0,150)}...</>}<br/>
					<span className="see-more">
						<Link to={`/article/${article.name}`}>See More</Link>
					</span>
				</li>
			})}
		</ul>
	)
}

export const SingleArticle = ({ match }) => {
	const name = match.params.name
	const article = articleContent.find( item => item.name === name)
	const defaultArticleInfoState = {upvotes:0, comments:[]}

	const [articleInfo, setArticleInfo] = useState(defaultArticleInfoState)
	useEffect(() => {
		const fetchData = async () => {
			let modifiedBody = defaultArticleInfoState
			const result  = await fetch(`/api/articles/${name}`) // using proxy
			if(result.ok) {
				const body = await result.json() // error if no name not found; also, printable
				if (body) modifiedBody = {upvotes: body.upvotes || 0, comments: body.comments}
			}else{
				console.log("Ensure that the backend server is running")
			}
			setArticleInfo(modifiedBody) //setArticleInfo(body) would work if upvotes wasn't nullable
		}
		fetchData()
	}, [name]) // empty means only when page loads, "name" means every time name in URL changes

	if(!article) return <NotFound />
	return(
		<>
			<h2>{article.title}</h2>
			{article.content.map((paragraph, key) => { return <p key={key}>{paragraph}</p> })}
			<UpvoteSection articleName={article.name} upvotes={articleInfo.upvotes} setArticleInfo={setArticleInfo}/>
			<div className="article-below-section">
				<div>
					<AddCommentForm articleName={article.name} setArticleInfo={setArticleInfo}/>
					<CommentsList comments={articleInfo.comments}/>
				</div>

				<div>
					<h4>Other Articles</h4>
					<div className="article-below-section-content">
						<ArticleList articleList={articleContent.filter(a => a.name !== article.name)} />
					</div>
				</div>
			</div>
		</>
	)
}


// Comments
const CommentsList = ( {comments} ) => {
	return(
		<>
			<h4>Comments:</h4>
			<div className="article-below-section-content">
				<ul>
					{comments.length > 0 ? comments.map( (comment, key) => {
						return(
							<li key={key}>
								<p>
									<strong>{comment.username}</strong><br/>
									{comment.text}
								</p>
							</li>)
					}) : <p>No comments to show.</p>}
				</ul>
			</div>
		</>
	)
}

const AddCommentForm = ( {articleName, setArticleInfo} ) => {
	const [username, setUsername] = useState("")
	const [commentText, setCommentText] = useState("")

	const addComment = async () => {
	  const response = await fetch(`/api/articles/${articleName}/add-comment`,{
			method:"POST",
		  headers: {"Content-type":'application/json'},
		  body: JSON.stringify({
			  username:username, text:commentText,
		  })
	  })
		if(response.ok){
			const body = await response.json()
			console.log({body})
			setArticleInfo(body)
			setUsername("")
			setCommentText("")
		}
	}
  return(
		<div id="add-comment-form">
			<h4>Add Comment</h4>
			<div className="article-below-section-content">
				<br/><label>Name:</label>
				<input type="text" onChange={(e) => setUsername(e.target.value)} value={username}/>
				<label>Comment</label>
				<textarea onChange={(e) => setCommentText(e.target.value)} rows="4" cols="50" value={commentText}/>
				<br/><button className="btn-theme" type="button" onClick={() => addComment()}>Post!</button>
			</div>
		</div>
  )
}

// Upvotes
const UpvoteSection = ( {upvotes, articleName, setArticleInfo} ) => {
	const addUpvote = async () => {
		const result  = await fetch(`/api/articles/${articleName}/upvote`, {method:'PUT'})
		if(result.ok){
			const body    = await result.json()
			setArticleInfo(body)
		}
	}

	return(<p>
		This post has been upvoted {upvotes} time{upvotes !== 1 && 's'}.
		&nbsp;<button className="btn-theme" type="button" onClick={() => addUpvote()}>Upvote!</button>
	</p>)
}
